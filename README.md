# Achieve
Achive is an achievement implementation and manager library based on Dovyski/Achieve's (https://github.com/Dovyski/Achieve) idea. 
To understanding the principle of operation please see the fallowing article: https://gamedevelopment.tutsplus.com/tutorials/how-to-code-unlockable-achievements-for-your-game-a-simple-approach--gamedev-6012

## Getting started

### Installation

```
composer require cay89/achieve
```

### Create your own Property and Achievement class

Because of flexibility and support application specific behaviours, I used interfaces and traits 
to implements business logic of the library. So just implement my interfaces and use my traits in your own class 
and override them if you need it.

#### Property

```php
use cay89\Achieve\PropertyInterface;
use cay89\Achieve\PropertyTrait;

class Property implements PropertyInterface {
    use PropertyTrait;

    // Database, application-specific operations etc.
}
```

#### Achievement

```php
use cay89\Achieve\AchievementInterface;
use cay89\Achieve\AchievementTrait;

class Achievement implements AchievementInterface {
    use AchievementTrait;

    // Database, application-specific operations etc.
}
```

### Define properties

1. The name of the property.
2. The function that verify whether this condition is met.
3. (optional) The parameters that will be passed to the "condition" function.
4. (optional) The tags of the property.

```php
$property = new Property('Greater then 10', function($params) {
    return ($params['value'] > 10);
}, ['value' => 25], ['level1']);
```

### Define achievements

1. The name of the achievement.
2. If these properties are "active", then achievement will be unlocked. 

```php
$achievement1 = new Achievement('Achievement 1', [$property1, $property2]);
```

### Achieve class usage

Instantiate the Achieve class with Achievement objects and call **check()** method to verify the requirements of the achievements.
It returns the unlocked ones.

```php
$achieve = new Achieve([$achievement1, $achievement2]);
$achieve->check();
```

Please see the following path for more examples: **example/example.php**