<?php
/**
 * Created by cay89.
 */

namespace cay89\Achieve\Example;

use cay89\Achieve\AchievementInterface;
use cay89\Achieve\AchievementTrait;

class Achievement implements AchievementInterface {
    use AchievementTrait;

    // Database, application-specific operations etc.
}