<?php
/**
 * Created by cay89.
 */
namespace cay89\Achieve\Example;

use cay89\Achieve\PropertyInterface;
use cay89\Achieve\PropertyTrait;

class Property implements PropertyInterface {
    use PropertyTrait;

    // Database, application-specific operations etc.
}