<?php
/**
 * Created by achieve.
 */

use cay89\Achieve\Achieve;
use cay89\Achieve\Example\Achievement;
use cay89\Achieve\Example\Property;

require_once __DIR__ . '/../vendor/autoload.php';

// Define properties #######################################################################################################################

// 1. The name of the property.
// 2. The function that verify whether this condition is met.
// 3. The parameters that will be passed to the "condition" function.
// 4. The tags of the property.
$property1 = new Property('Greater then 10', function($params) {
    return ($params['value'] > 10);
}, ['value' => 25], ['level1']);
// OR
$property2 = (new Property())->setName('Is even number')->setCondition(function($params) {
    return ($params['value'] % 2 == 0);
})->setConditionParams(['value' => 25])->setTags(['level2']);
// OR
$property3 = new Property();
$property3->setName('Divisible by 5');
$property3->setCondition(function($params) {
    return ($params['value'] % 5 == 0);
});
$property3->setConditionParams(['value' => 25]);
$property3->setTags(['level2']);

// Define achievements #####################################################################################################################

$achievement1 = new Achievement('Achievement 1', [$property1, $property3]);
// OR
$achievement2 = (new Achievement())->setName('Achievement 2')->setProperties([$property3]);
// OR
$achievement3 = (new Achievement())->setName('Achievement 2')->setProperties(['Third prop' => $property3]);
// OR
$achievement4 = (new Achievement())->setName('Achievement 4')->setProperty('Third prop', $property3);
// OR
$achievement5 = (new Achievement())->setName('Achievement 5')->addProperty($property3);
// OR
$achievement6 = (new Achievement())->setName('Achievement 6')->addProperty($property3, 'Third prop');

// Achieve class usage #####################################################################################################################

// Load the definition of Achievements to an Achieve object
$achieve1 = new Achieve([$achievement1, $achievement2]);
// OR
$achieve2 = (new Achieve())->setAchievements([$achievement1, $achievement2]);
// OR
$achieve3 = (new Achieve())->setAchievements(['this is the key' => $achievement1, $achievement2]);
// OR
$achieve4 = (new Achieve())->setAchievement('First achievement', $achievement1)->setAchievement('Second achievement', $achievement2);
// OR
$achieve5 = (new Achieve())->addAchievement($achievement1, 'optional key')->addAchievement($achievement2);
// OR
$achieve6 = (new Achieve())->addAchievement($achievement1)->addAchievement($achievement2);

$getInstance = function () use ($property1, $property2, $property3) {
    return new Achieve([
        new Achievement('Achievement 1', [clone $property1, clone $property3]),
        new Achievement('Achievement 2', ['property2' => clone $property2]),
    ]);
};

// Example 1 #####################

$achieve = $getInstance();
// Check the requirements of the achievements and return the unlocked ones
$unlockedAchievements = $achieve->check();
// Print: Achievement 1
array_map(function(Achievement $achievement) { echo "Example 1\n\r" . $achievement->getName() . "\n\r"; }, $unlockedAchievements);

// Example 2 #####################

$achieve = $getInstance();
// Check properties with "level2" tag only. This will allow you to define same properties in different context.
$unlockedAchievements = $achieve->check(['level2']);
if(empty($unlockedAchievements)) echo "Example 2\n\r" . '$unlockedAchievements is empty.' . "\n\r";

// Example 3 #####################

// Get properties by index or key easily
$achieve = $getInstance();
echo "Example 3\n\rProperty 1: " . $achieve->getProperty(0)->getName() . ' Property 2: '
    . $achieve->getProperty('property2')->getName(). "\n\r";
$properties = $achieve->getProperties();
echo "All properties: \n\r";
array_map(function(Property $property) { echo $property->getName() . "\n\r"; }, $properties);

// Example 4 #####################

// Set up properties manually
$achieve = $getInstance();
$unlockedAchievements = $achieve->check();
echo "Example 4\n\r";
// Print: Achievement 1
array_map(function(Achievement $achievement) { echo $achievement->getName() . "\n\r"; }, $unlockedAchievements);
echo "---\n\r";
$achieve->getProperty('property2')->setActive(true);
$unlockedAchievements = $achieve->check();
// Print: Achievement 2
array_map(function(Achievement $achievement) { echo $achievement->getName() . "\n\r"; }, $unlockedAchievements);