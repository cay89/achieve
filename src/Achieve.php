<?php
/**
 * Created by cay89.
 */

namespace cay89\Achieve;

/**
 * Class Achieve
 *
 * @package cay89\Achieve
 */
class Achieve {
    use FillTrait, PropertyHandlerTrait;

    /** @var AchievementInterface[] */
    protected $achievements = [];

    /**
     * Achieve constructor.
     *
     * @param AchievementInterface[] $achievements
     * @param PropertyInterface[] $properties
     */
    public function __construct(array $achievements = [], array $properties = []) {
        $this->fill(get_defined_vars());
        if(!empty($achievements)) {
            foreach ($achievements as $a) {
                $this->properties = array_merge($this->properties, $a->getProperties());
            }
        }
    }

    /**
     * @param AchievementInterface $achievement
     * @param null $key
     * @return $this
     */
    public function addAchievement(AchievementInterface $achievement, $key = null) {
        if($key) $this->achievements[$key] = $achievement;
        else $this->achievements[] = $achievement;

        return $this;
    }

    /**
     * @param mixed $key
     * @param AchievementInterface $achievement
     * @return $this
     */
    public function setAchievement($key, AchievementInterface $achievement) {
        $this->achievements[$key] = $achievement;

        return $this;
    }

    /**
     * @param $key
     * @return AchievementInterface
     */
    public function getAchievement($key) {
        return $this->achievements[$key];
    }

    /**
     * @param AchievementInterface[] $achievements
     * @return $this
     */
    public function setAchievements(array $achievements) {
        $this->achievements = $achievements;

        return $this;
    }

    /**
     * @return AchievementInterface[]
     */
    public function getAchievements() {
        return $this->achievements;
    }

    /**
     * Check the requirements of the achievements and return the unlocked ones.
     *
     * @param array $tags
     * @param array $conditionParams
     * @return AchievementInterface[]
     */
    public function check(array $tags = [], array $conditionParams = []) {
        $return = [];
        if(!empty($this->achievements)) {
            foreach($this->achievements as $key => $a) {
                if(!$a->getUnlocked()) {
                    $activeProps = 0;
                    foreach($a->getProperties() as $p) {
                        if(!empty($conditionParams)) $p->setConditionParams($conditionParams);
                        if((empty($tags) || !empty(array_intersect($tags, $p->getTags()))) && $p->isActive()) {
                            $activeProps++;
                        }
                    }
                    if($activeProps == count($a->getProperties())) {
                        $a->setUnlocked(true);
                        $return[$key] = $a;
                    }
                }
            }
        }

        return $return;
    }
}