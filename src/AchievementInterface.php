<?php
/**
 * Created by cay89.
 */

namespace cay89\Achieve;

/**
 * Class Achievement
 *
 * @package cay89\Achieve
 */
interface AchievementInterface {

    /**
     * AchievementInterface constructor.
     *
     * @param string $name
     * @param array $properties
     * @param bool $unlocked
     */
    public function __construct($name = '', array $properties = [], $unlocked = false);

    /**
     * @param string $name
     * @return AchievementInterface
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getName();

    /**
     * @param bool $unlocked
     * @return AchievementInterface
     */
    public function setUnlocked($unlocked);

    /**
     * @return bool
     */
    public function getUnlocked();

    /**
     * @param PropertyInterface $property
     * @param null $key
     * @return $this
     */
    public function addProperty(PropertyInterface $property, $key = null);

    /**
     * @param mixed $key
     * @param PropertyInterface $property
     * @return $this
     */
    public function setProperty($key, PropertyInterface $property);

    /**
     * @param $key
     * @return PropertyInterface
     */
    public function getProperty($key);

    /**
     * @param PropertyInterface|PropertyInterface[] $properties
     * @return $this
     */
    public function setProperties(array $properties);

    /**
     * @return PropertyInterface[]
     */
    public function getProperties();
}