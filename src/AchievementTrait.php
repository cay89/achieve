<?php
/**
 * Created by cay89.
 */

namespace cay89\Achieve;

/**
 * Class Achievement
 *
 * @package cay89\Achieve
 */
trait AchievementTrait {
    use FillTrait, PropertyHandlerTrait;

    protected $name;
    protected $unlocked;

    public function __construct($name = '', array $properties = [], $unlocked = false) {
        $this->fill(get_defined_vars());
    }

    /**
     * @param string $name
     * @return AchievementInterface
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName() {
        return (string)$this->name;
    }

    /**
     * @param bool $unlocked
     * @return AchievementInterface
     */
    public function setUnlocked($unlocked) {
        $this->unlocked = (bool)$unlocked;

        return $this;
    }

    /**
     * @return bool
     */
    public function getUnlocked() {
        return $this->unlocked;
    }
}