<?php
/**
 * Created by cay89.
 */

namespace cay89\Achieve;

trait FillTrait {
    public function fill(array $params) {
        foreach($params as $var => $value) {
            if(property_exists($this, $var)) $this->$var = $value;
        }
    }
}