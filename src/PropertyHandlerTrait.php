<?php
/**
 * Created by achieve.
 */

namespace cay89\Achieve;

/**
 * Class PropertyHandlerTrait
 *
 * @package cay89\Achieve
 */
trait PropertyHandlerTrait {

    /** @var PropertyInterface[] */
    protected $properties = [];

    /**
     * @param PropertyInterface $property
     * @param null $key
     * @return $this
     */
    public function addProperty(PropertyInterface $property, $key = null) {
        if($key) $this->properties[$key] = $property;
        else $this->properties[] = $property;

        return $this;
    }

    /**
     * @param mixed $key
     * @param PropertyInterface $property
     * @return $this
     */
    public function setProperty($key, PropertyInterface $property) {
        $this->properties[$key] = $property;

        return $this;
    }

    /**
     * @param $key
     * @return PropertyInterface
     */
    public function getProperty($key) {
        return $this->properties[$key];
    }

    /**
     * @param PropertyInterface[] $properties
     * @return $this
     */
    public function setProperties(array $properties) {
        $this->properties = $properties;

        return $this;
    }

    /**
     * @return PropertyInterface[]
     */
    public function getProperties() {
        return $this->properties;
    }
}