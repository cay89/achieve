<?php
/**
 * Created by achieve.
 */

namespace cay89\Achieve;

/**
 * Interface PropertyInterface
 *
 * @package cay89\Achieve
 */
interface PropertyInterface {

    /**
     * PropertyInterface constructor.
     *
     * @param string $name
     * @param callable|null $condition
     * @param array $conditionParams
     * @param array $tags
     * @param bool $active
     */
    public function __construct($name = '', callable $condition = null, array $conditionParams = [], array $tags = [], $active = false);

    /**
     * @param $name
     * @return PropertyInterface
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getName();

    /**
     * @param callable $condition
     * @return PropertyInterface
     */
    public function setCondition(callable $condition);

    /**
     * @return callable
     */
    public function getCondition();

    /**
     * @param array $params
     * @return PropertyInterface
     */
    public function setConditionParams(array $params);

    /**
     * @return array
     */
    public function getConditionParams();

    /**
     * @param bool $active
     * @return PropertyInterface
     */
    public function setActive($active);

    /**
     * @return bool
     */
    public function getActive();

    /**
     * @param array $tags
     * @return PropertyInterface
     */
    public function setTags(array $tags);

    /**
     * @return array
     */
    public function getTags();

    /**
     * Verify property completion.
     *
     * @return bool
     */
    public function isActive();
}