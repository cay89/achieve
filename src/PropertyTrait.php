<?php
/**
 * Created by cay89.
 */

namespace cay89\Achieve;

/**
 * Class Property
 *
 * @package cay89\Achieve
 */
trait PropertyTrait {
    use FillTrait;

    protected $name;
    /** @var callable */
    protected $condition;
    protected $conditionParams = [];
    protected $active;
    protected $tags = [];

    /**
     * Property constructor.
     *
     * @param string $name
     * @param callable|null $condition
     * @param array $conditionParams
     * @param bool $active
     * @param array $tags
     * @internal param int $value
     * @internal param int $initialValue
     */
    public function __construct($name = '', callable $condition = null, array $conditionParams = [], array $tags = [], $active = false) {
        $this->fill(get_defined_vars());
    }

    /**
     * @param string $name
     * @return PropertyInterface
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName() {
        return (string)$this->name;
    }

    /**
     * @param callable $condition
     * @return PropertyInterface
     */
    public function setCondition(callable $condition) {
        $this->condition = $condition;

        return $this;
    }

    /**
     * @return callable
     */
    public function getCondition() {
        return $this->condition;
    }

    /**
     * @param array $params
     * @return PropertyInterface
     */
    public function setConditionParams(array $params) {
        $this->conditionParams = $params;

        return $this;
    }

    /**
     * @return array
     */
    public function getConditionParams() {
        return $this->conditionParams;
    }

    /**
     * @param bool $active
     * @return PropertyInterface
     */
    public function setActive($active) {
        $this->active = (bool)$active;

        return $this;
    }

    /**
     * @return bool
     */
    public function getActive() {
        return (bool)$this->active;
    }

    /**
     * @param array $tags
     * @return PropertyInterface
     */
    public function setTags(array $tags) {
        $this->tags = $tags;

        return $this;
    }

    /**
     * @return array
     */
    public function getTags() {
        return $this->tags;
    }

    /**
     * Verify property completion.
     *
     * @return bool
     */
    public function isActive() {
        if($this->active) return true;
        else return $this->active = ((is_callable($this->condition))
            ? (bool)call_user_func($this->condition, $this->conditionParams) : false);
    }
}