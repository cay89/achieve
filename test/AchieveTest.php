<?php
/**
 * Created by cay89.
 */

namespace cay89\Achieve\Test;

use cay89\Achieve\Achieve;
use cay89\Achieve\Example\Achievement;
use cay89\Achieve\Example\Property;
use PHPUnit\Framework\TestCase;

class AchieveTest extends TestCase {

    /**
     * @return array
     */
    public function getData() {
        $testValue = 25;

        $property1 = new Property('Greater then 10', function($params) {
            return ($params['value'] > 10);
        }, ['value' => $testValue], ['foo']);
        $property2 = new Property('Is even number', function($params) {
            return ($params['value'] % 2 == 0);
        }, ['value' => $testValue], ['bar']);
        $property3 = new Property('Divisible by 5', function($params) {
            return ($params['value'] % 5 == 0);
        }, ['value' => $testValue], ['bar']);

        return [
            [
                new Achieve([
                    new Achievement('Achievement 1', [$property1, $property3]),
                    new Achievement('Achievement 2', [$property2])
                ]),
                [],
                [true, false],
                [0, 1]
            ],
            [
                new Achieve([
                    'a1' => new Achievement('Achievement 1', [$property1, $property3]),
                    'a2' => new Achievement('Achievement 2', [$property2])
                ]),
                ['bar'],
                [false, false],
                ['a1', 'a2']
            ]
        ];
    }

    /**
     * @dataProvider getData
     * @param Achieve $achieve
     * @param array $tags
     * @param array $asserts
     * @param array $keys
     */
    public function testCheck(Achieve $achieve, array $tags = [], array $asserts = [], array $keys = []) {
        $achieve->check($tags);
        $this->assertEquals($asserts[0], $achieve->getAchievement($keys[0])->getUnlocked());
        $this->assertEquals($asserts[1], $achieve->getAchievement($keys[1])->getUnlocked());
    }

    public function testCheckConditionParams() {
        $achieve = new Achieve([
            new Achievement('Achievement 1', [
                new Property('Greater then 10', function($params) {
                    return (array_key_exists('value', $params) && $params['value'] > 10);
                })
            ]),
        ]);
        $achieve->check();
        $this->assertEquals(false, $achieve->getAchievement(0)->getUnlocked());
        $achieve->check([], ['value' => 20]);
        $this->assertEquals(true, $achieve->getAchievement(0)->getUnlocked());
    }
}
