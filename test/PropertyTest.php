<?php
/**
 * Created by cay89.
 */

namespace cay89\Achieve\Test;

use cay89\Achieve\Example\Property;
use PHPUnit\Framework\TestCase;

class PropertyTest extends TestCase {

    public function testIsActive() {
        $property = new Property();
        $this->assertEquals(false, $property->isActive());

        $property->setActive(true);
        $this->assertEquals(true, $property->isActive());

        $property->setActive(false)->setCondition(function() {
            return true;
        });
        $this->assertEquals(true, $property->isActive());

        $property->setActive(false)->setConditionParams(['foo' => 'bar'])->setCondition(function($params) {
            return ($params['foo'] == 'bar');
        });
        $this->assertEquals(true, $property->isActive());
    }
}
